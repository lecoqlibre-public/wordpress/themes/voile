class LecoqlibreSwitcher extends HTMLElement {

    static get observedAttributes() {
      return [ 'for' ];
    }
  
    constructor() {
      super();
      var template = document.createElement( 'template' );
      template.innerHTML = `
        <style type="text/css">
          ::slotted(*) {
            display: inline;
            cursor: pointer;
          }
        </style>
        <slot name="switcher"></slot>`;
      this.attachShadow( { mode: 'open' } );
      this.shadowRoot.appendChild( template.content.cloneNode( true ) );
  
      this.shadowRoot.querySelector( '[name="switcher"]' ).addEventListener( 'click', e => {
        this.toggle();
      } );
  
    }

    isEnabled() {
        return this.hasAttribute( 'enabled' );
    }
  
    toggle() {
        if( this.hasAttribute( 'for' ) ) {
            var target = document.getElementById( this.getAttribute( 'for' ) );
            target.toggle();
            if( target.isEnabled() )
                this.setAttribute( 'enabled', '' );
            else this.removeAttribute( 'enabled' );
        }
    }
  
    connectedCallback() {
      console.log( "lecoqlibre-switcher connected" );
    }
  
  }
  
  customElements.define( 'lecoqlibre-switcher', LecoqlibreSwitcher );
  // setTimeout( () => customElements.define( 'lecoqlibre-switcher', LecoqlibreSwitcher ), 1000 );
  