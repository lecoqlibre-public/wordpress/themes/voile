<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Diyseeds
 * @since Diyseeds 2.0
 */

?>

<?php wp_footer(); ?>

<?php if ( dynamic_sidebar( 'lecoqlibre-voile-bottom-pane' ) ) : else : endif; ?>

</body>
</html>
