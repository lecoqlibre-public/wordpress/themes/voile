<?php // if ( dynamic_sidebar('diyseeds-main-toolbar') ) : else : endif; ?>

<?php while ( have_posts() ) : the_post(); ?>

    <?php echo the_content(); ?>

    <?php
    // If comments are open or there is at least one comment, load up the comment template.
    if ( comments_open() || get_comments_number() ) {
        comments_template();
    }
    ?>
<?php endwhile; ?>

<?php // if ( dynamic_sidebar('diyseeds-main-toolbar') ) : else : endif; ?>