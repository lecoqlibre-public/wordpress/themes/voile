<lecoqlibre-switch id="lecoqlibre-voile-sidemenu">
    <div slot='content'>
        <?php if ( function_exists( 'the_custom_logo' ) ) the_custom_logo(); ?>
        <nav id='menus'>
            <?php 
                $menuCount = get_theme_mod( 'sidemenu_count', 3 );
                for( $i = 1; $i <= $menuCount; $i++ )
                    wp_nav_menu( array( 'container' => false, 'fallback_cb' => false, 'theme_location' => "lecoqlibre-voile-sidemenu-{$i}" ) ); 
            ?>
        </nav>
    </div>
</lecoqlibre-switch>