<lecoqlibre-switcher for="lecoqlibre-voile-sidemenu">
    <nav slot="switcher">
        <?php echo get_theme_mod( 'sidemenu_switcher_content', '<span>+</span>' ); ?>
    </nav>
</lecoqlibre-switcher>