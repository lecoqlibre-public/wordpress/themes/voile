<?php
/**
 * The header.
 *
 * This is the template that displays all of the <head> section and everything up until main.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Diyseeds
 * @since Diyseeds 2.0
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<script async type="text/javascript" src="<?php echo get_stylesheet_directory_uri() ;?>/script.min.js"></script>
	<?php wp_head(); ?>
</head>

<body <?php body_class( [ 'voile' ] ); ?>>
<?php wp_body_open(); ?>
<?php if ( dynamic_sidebar( 'lecoqlibre-voile-top-pane' ) ) : else : endif; ?>
