<?php

/** Enqueue theme style or child theme style. */
add_action( 'wp_enqueue_scripts', function() {
  $styleFilename = '/style.min.css';
  $childStyleFilename = get_stylesheet_directory_uri() . $styleFilename;
  $styleFile = file_exists( $childStyleFilename ) ? $childStyleFilename : get_template_directory_uri() . $styleFilename;
  wp_enqueue_style( 'lecoqlibre-voile-style', $styleFile, array(), wp_get_theme()->get( 'Version' ) );
} );

add_action( 'init', function() {
  $menus = array();
  $menuCount = get_theme_mod( 'sidemenu_count', 3 );

  for( $i = 1; $i <= $menuCount; $i++ )
    $menus[ "lecoqlibre-voile-sidemenu-{$i}" ] = sprintf( __( 'Sidemenu #%d' ), $i );

  register_nav_menus( apply_filters( 'lecoqlibre-voile_register_nav_menus', $menus ) );
} );

/** Add Gutenberg voile patterns. */
add_action( 'init', function() {
  /** Get the default width of the content column from customizer. */
  $columnWidth = get_theme_mod( 'content_default_width', '400px' );

  /** Get the default width of the social bar column from customizer. */
  $socialWidth = get_theme_mod( 'socialbar_default_width', '28px' );

  $extraWidth = get_theme_mod( 'content_extra_default_width', '250px' );

  $gutenslider = "<!-- wp:eedee/block-gutenslider {\"contentMode\":\"fixed\",\"dots\":false,\"dotsMd\":false,\"dotsSm\":false,\"className\":\"lecoqlibre-voile-background\"} /-->\n\n";
  $article = "<!-- wp:group {\"tagName\":\"article\",\"className\":\"lecoqlibre-voile-content\"} -->\n<article class=\"wp-block-group lecoqlibre-voile-content\"><!-- wp:post-title {\"level\":1} /--></article>\n<!-- /wp:group -->";
  $content = "<!-- wp:column {\"width\":\"{$columnWidth}\",\"className\":\"bg-opacity-80\"} -->\n<div class=\"wp-block-column bg-opacity-80\" style=\"flex-basis:{$columnWidth}\">{$article}</div>\n<!-- /wp:column -->\n\n";
  $socialBar = "<!-- wp:column {\"width\":\"{$socialWidth}\",\"className\":\"lecoqlibre-voile-socialbar bg-opacity-80\"} -->\n<div class=\"wp-block-column lecoqlibre-voile-socialbar bg-opacity-80\" style=\"flex-basis:{$socialWidth}\"><!-- wp:social-links {\"openInNewTab\":true} -->\n<ul class=\"wp-block-social-links\"></ul>\n<!-- /wp:social-links --></div>\n<!-- /wp:column -->";
  $foreground = "<!-- wp:columns {\"className\":\"lecoqlibre-voile-foreground\"} -->\n<div class=\"wp-block-columns lecoqlibre-voile-foreground\">{$content}{$socialBar}</div>\n<!-- /wp:columns -->";
  $main = "<!-- wp:group {\"tagName\":\"main\",\"className\":\"lecoqlibre-voile-main\"} -->\n<main class=\"wp-block-group lecoqlibre-voile-main\">{$gutenslider}{$foreground}</main>\n<!-- /wp:group -->";
  
  register_block_pattern(
    'lecoqlibre-voile/content-social',
    array(
      'title'       => __( 'Content with social bar', 'lecoqlibre-voile' ),
      'description' => _x( 'Carousel background with content and social bar.', 'Block pattern description', 'lecoqlibre-voile' ),
      'content'     => $main,
      'categories' => [ 'columns' ],
      'keywords' => [ 'voile' ],
      )
    );
    
  $extra = "<!-- wp:column {\"width\":\"{$extraWidth}\",\"className\":\"bg-opacity-80\"} -->\n<div class=\"wp-block-column bg-opacity-80\" style=\"flex-basis:{$extraWidth}\"></div>\n<!-- /wp:column -->";
  $foreground2 = "<!-- wp:columns {\"className\":\"lecoqlibre-voile-foreground\"} -->\n<div class=\"wp-block-columns lecoqlibre-voile-foreground\">{$content}{$extra}{$socialBar}</div>\n<!-- /wp:columns -->";
  $main2 = "<!-- wp:group {\"tagName\":\"main\",\"className\":\"lecoqlibre-voile-main\"} -->\n<main class=\"wp-block-group lecoqlibre-voile-main\">{$gutenslider}{$foreground2}</main>\n<!-- /wp:group -->";
  
  register_block_pattern(
      'lecoqlibre-voile/content-social2',
      array(
          'title'       => __( 'Two columns with social bar', 'lecoqlibre-voile' ),
          'description' => _x( 'Carousel background with two columns and social bar.', 'Block pattern description', 'lecoqlibre-voile' ),
          'content'     => $main2,
          'categories' => [ 'columns' ],
          'keywords' => [ 'voile' ],
      )
  );
} );

/** Initialize theme. */
add_action( 'after_setup_theme', function() {
  add_theme_support( 'title-tag' );
  // add_theme_support( 'admin-bar' );
  add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption', 'style', 'script' ) );
  add_theme_support( 'menus' );
  add_theme_support( 'wp-block-styles'  );
  add_theme_support( 'widgets'  );
  add_theme_support( 'widgets-block-editor' );
  // add_theme_support( 'editor-color-palette' );

  // Post thumbnail supported for social image
  add_theme_support( 'post-thumbnails' );
  set_post_thumbnail_size( 480, 270 );

  // Custom logo
  $defaults = array(
    'width'                => 200,
    'height'               => 200,
    'flex-height'          => true,
    'flex-width'           => true,
    'header-text'          => array( 'site-title', 'site-description' ),
    'unlink-homepage-logo' => true, 
  );
  add_theme_support( 'custom-logo', $defaults );
} );

/** Insert social bar into web component slot. */
add_filter( 'render_block_core/column', function( string $block_content, array $block ) {
  if( ! isset( $block[ 'attrs' ][ 'className' ] ) )
    return $block_content;

  $classes = explode( ' ', $block[ 'attrs' ][ 'className' ] ); // classes should be separated with space

  if( ! in_array( 'lecoqlibre-voile-socialbar', $classes ) )
    return $block_content;
    
  $id = 'lecoqlibre-voile-socialbar';
  $width = $block[ 'attrs' ][ 'width' ];
  $block_content = "<div slot='content' " . substr( $block_content, 4 );

  return "<lecoqlibre-switch id='{$id}' class='wp-block-column' style='flex-basis:{$width};'>{$block_content}</lecoqlibre-switch>";
}, 10, 2 );

// function diyseeds_setup() {
//   add_theme_support( 'widgets' );
//   load_theme_textdomain('diyseeds', get_template_directory_uri() . '/languages');
// }
// add_action( 'after_setup_theme', 'diyseeds_setup' );


add_action( 'widgets_init', function () {
  // Top pane
	register_sidebar(
		array(
      /* translators: Name of the main bar used to display various tools (widgets) */
			'name'          => esc_html_x( 'Top pane', 'sidebar', 'lecoqlibre-voile' ),
			'id'            => 'lecoqlibre-voile-top-pane',
      /* translators: Description of the main bar used to display various tools (widgets) */
			'description'   => esc_html_x( 'Widgets will be added at top of website over content.', 'sidebar', 'lecoqlibre-voile' ),
      'before_sidebar' => '<lecoqlibre-switch id="lecoqlibre-voile-top-pane" class="lecoqlibre-voile-pane" enabled=""><aside slot="content" id="top-pane" class="pane-sidebar">',
      'after_sidebar' => '</aside></lecoqlibre-switch>',
      'before_widget' => '',
      'after_widget' => '',
		)
	);

  // Bottom pane
	register_sidebar(
		array(
      /* translators: Name of the main bar used to display various tools (widgets) */
			'name'          => esc_html_x( 'Bottom pane', 'sidebar', 'lecoqlibre-voile' ),
			'id'            => 'lecoqlibre-voile-bottom-pane',
      /* translators: Description of the main bar used to display various tools (widgets) */
			'description'   => esc_html_x( 'Widgets will be added at bottom of website over content.', 'sidebar', 'lecoqlibre-voile' ),
      'before_sidebar' => '<lecoqlibre-switch id="lecoqlibre-voile-bottom-pane" class="lecoqlibre-voile-pane" enabled=""><aside slot="content" id="bottom-pane" class="pane-sidebar">',
      'after_sidebar' => '</aside></lecoqlibre-switch>',
      'before_widget' => '',
      'after_widget' => '',
		)
	);
} );


add_action( 'customize_register', function( $wp_customize ) {
  $wp_customize->add_setting( 'sidemenu_count', array(
    'default' => 3,
    'sanitize_callback' => 'intval',
  ) );

  $wp_customize->add_setting( 'socialbar_default_width', array(
    'default' => '28px',
    'sanitize_callback' => 'sanitize_text_field',
  ) );

  $wp_customize->add_setting( 'content_default_width', array(
    'default' => '400px',
    'sanitize_callback' => 'sanitize_text_field',
  ) );
} );