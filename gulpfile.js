const { Transform } = require('stream');

var gulp       = require('gulp');
var less       = require('gulp-less');
var minifyCSS  = require('gulp-csso');
var concat     = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var insert     = require('gulp-insert');
var minify     = require('gulp-minify');

class CSSFolderProcess extends Transform {
    constructor(distFolder) {
        super({objectMode: true});
        this.distFolder = distFolder;
    }

    _transform(file, encoding, callback) {
        if (file.isDirectory()) {
            let distDir          = this.distFolder;
            let distFileBaseName = 'style.min'; //filterName + '.min';

            gulp.src( file.path + '/**/*.less' )
                .pipe(less())
                .pipe(concat(distFileBaseName + '.css'))
                .pipe(minifyCSS())
                .pipe(gulp.dest(distDir));
        }
        callback();
    }
}

class JSFolderProcess extends Transform {
    constructor(distFolder) {
        super({objectMode: true});
        this.distFolder = distFolder;
    }

    _transform(file, encoding, callback) {
        if (file.isDirectory()) {
            let distDir          = this.distFolder;
            let distFileBaseName = 'script.min'; //filterName + '.min';

            gulp.src( file.path + '/**/*.js' )
                .pipe(concat(distFileBaseName + '.js'))
                .pipe( minify( {
                    ext:{
                        src:'-debug.js',
                        min:'.js'
                    }
                } ) )
                .pipe(gulp.dest(distDir));
        }
        callback();
    }
}

gulp.task( 'css', function() {
    return gulp.src( 'src/assets/' ).pipe( new CSSFolderProcess( './src/' ) );
});

gulp.task( 'js', function() {
    return gulp.src( 'src/assets/' ).pipe( new JSFolderProcess( './src/' ) );
});

gulp.task('watch', function() {
    var watcherCSS = gulp.watch( 'src/assets/**/*.less', gulp.series( 'css' ) );
    var watcherJS = gulp.watch( 'src/assets/**/*.js', gulp.series( 'js' ) );
    watcherCSS.on( 'change', function( path ) {
        console.log('[CSS] File ' + path + ' was changed');
    });
    watcherJS.on( 'change', function( path ) {
        console.log( '[JS] File ' + path + ' was changed' );
    });
});
